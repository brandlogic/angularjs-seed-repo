'use strict';

angular.module('myApp', [
  'ui.router',
  'app.homePages'
  ])

  .run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }])

  .config(['$locationProvider', '$stateProvider', '$urlRouterProvider', '$httpProvider', function($locationProvider, $stateProvider, $urlRouterProvider, $httpProvider) {
    // $locationProvider.html5Mode(true);
    //$httpProvider.interceptors.push('resourceInterceptor');
    // remove trailing slash
    $urlRouterProvider.rule(function($injector, $location) {
      var path = $location.path(), search = $location.search();
      if (path[path.length-1] === '/') {
        path = path.slice(0, -1);
        if (search === {}) {
          return path;
        }
        else {
          var params = [];
          angular.forEach(search, function(v, k){
            params.push(k + '=' + v);
          });
          return path + '?' + params.join('&');
        }
      }
    });

    // Define routes
    $stateProvider
      .state('test', {
        abstract: true,
        url: '/{test}',
        template: '<ui-view/>'
      });
  }]);
