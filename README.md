# Brandlogic AngularJS Seed Repo

A starter AngularJS repository building off the fine work already done by [Yearofmoo](https://github.com/yearofmoo/angularjs-seed-repo). It adds support for Sass/Compass, build script for performance, and deploying to S3.

## Installation

1. `npm install -g grunt-cli`
2. `npm install`
3. `grunt install`

## Development

1. `grunt dev`
2. Go to: `http://localhost:8888`

## Testing

### Run all tests with
`grunt test`

### Unit Testing

#### Single run tests
`grunt test:unit`

#### Auto watching tests
`grunt autotest:unit`

### End to End Testing (Protractor)

#### Single run tests
`grunt test:e2e`

#### Auto watching tests
`grunt autotest:e2e`

### Coverage Testing

`grunt coverage`
