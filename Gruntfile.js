module.exports = function(grunt) {

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({

    clean: {
      build: [
        'build'
      ]
    },

    compass: {
      options: {
        sassDir: './app/styles',
        force: true,
        cssDir: './build/assets',
        // require: ['breakpoint', 'animation']
      },

      dev:{
        options: {
          environment: 'development'
        }
      },
      prod: {
        options: {
          environment: 'production'
        }
      }
    },

    concat: {
      scripts: {
        options: {
          separator: ';'
        },
        dest: './build/assets/app.js',
        src: [
          'bower_components/angular/angular.js',
          'bower_components/angular-resource/angular-resource.js',
          'bower_components/angular-ui-router/release/angular-ui-router.js',
          'bower_components/angular-animate/angular-animate.js',
          'app/**/*.js',
          '!app/**/*.spec.js'
        ]
      },
    },

    connect: {
      options: {
        base: 'build/',
        hostname: '0.0.0.0'//'10.10.3.147'
      },
      webserver: {
        options: {
          port: 8888,
          keepalive: true
        }
      },
      devserver: {
        options: {
          port: 8888
        }
      },
      deploytest: {
        options: {
          port: 8888,
          keepalive: true
        }
      },
      testserver: {
        options: {
          port: 9999
        }
      },
      coverage: {
        options: {
          base: 'coverage/',
          port: 5555,
          keepalive: true
        }
      }
    },

    copy: {
      prod: {
        files: [
          // includes files within path and its sub-directories
          {expand: true, cwd: 'app/', src: ['**/*.html'], dest: 'build/'},

          // includes files within path and its sub-directories
          {expand: true, cwd: 'images/', src: ['**'], dest: 'build/images/'},
        ]
      },
    },

    hashres: {
      options: {
        fileNameFormat: '${name}.${hash}.${ext}',
        renameFiles: true
      },
      css: {
        src: [ 'build/**/*.css' ],
        dest: ['build/**/*.html', 'build/**/*.json']
      },
      js: {
        src: [ 'build/**/*.js' ],
        dest: 'build/**/*.html'
      },
      images: {
        src: [
          'build/**/*.png',
          'build/**/*.jpg',
          'build/**/*.svg',
          'build/**/*.ico'
        ],
        dest: [
          'build/**/*.html',
          'build/**/*.js',
          'build/**/*.css',
          'build/**/*.md',
          'build/**/*.json'
        ]
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'app/{,*/}*.js'
      ]
    },

    karma: {
      unit: {
        configFile: './test/karma-unit.conf.js',
        autoWatch: false,
        singleRun: true
      },
      unit_auto: {
        configFile: './test/karma-unit.conf.js',
        autoWatch: true,
        singleRun: false
      },
      unit_coverage: {
        configFile: './test/karma-unit.conf.js',
        autoWatch: false,
        singleRun: true,
        reporters: ['progress', 'coverage'],
        preprocessors: {
          'app/*.js': ['coverage']
        },
        coverageReporter: {
          type : 'html',
          dir : 'coverage/'
        }
      },
    },

    open: {
      devserver: {
        path: 'http://localhost:8888'
      },
      coverage: {
        path: 'http://localhost:5555'
      }
    },

    protractor: {
      options: {
        keepAlive: true,
        configFile: "./test/protractor.conf.js"
      },
      singlerun: {},
      auto: {
        keepAlive: true,
        options: {
          args: {
            seleniumPort: 4444
          }
        }
      }
    },

    aws: grunt.file.readJSON('.grunt-aws.json'),
    s3: {
      options: {
        key: '<%= aws.key %>',
        secret: '<%= aws.secret %>',
        bucket: '[INSERT BUCKET HERE]',
        access: 'public-read'
      },
      prod: {
        // Files to be uploaded.
        upload: [
          {
            // CSS
            src: 'build/**/*.css',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Cache-Control": "private",
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // JS
            src: 'build/**/*.js',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Cache-Control": "private",
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // SVG
            src: 'build/**/*.svg',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Cache-Control": "private",
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // ICO
            src: 'build/**/*.ico',
            dest: '/',
            rel: 'build',
            options: {
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // PNG
            src: 'build/**/*.png',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // JPG
            src: 'build/**/*.jpg',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // GIF
            src: 'build/**/*.gif',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true,
              headers: {
                // Two Year cache policy (1000 * 60 * 60 * 24 * 730)
                "Expires": new Date(Date.now() + 63072000000).toUTCString()
              }
            }
          },
          {
            // HTML
            src: 'build/**/*.html',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true
            }
          },
          {
            // JSON
            src: 'build/**/*.json',
            dest: '/',
            rel: 'build',
            options: {
              gzip: true
            }
          },
        ],
      }
    },

    shell: {
      options: {
        stdout: true
      },
      selenium: {
        command: './selenium/start',
        options: {
          stdout: false,
          async: true
        }
      },
      protractor_install: {
        command: 'node ./node_modules/protractor/bin/webdriver-manager update'
      },
      npm_install: {
        command: 'npm install'
      }
    },

    watch: {
      options : {
        livereload: true
      },
      css: {
        files: ['app/**/*.scss', 'styles/**/*.scss'],
        tasks: ['compass:dev']
      },
      html: {
        files: ['app/**/*.html'],
        tasks: ['copy']
      },
      js: {
        files: ['app/**/*.js'],
        tasks: ['concat']
      },
      protractor: {
        files: ['app/scripts/**/*.js','test/e2e/**/*.js'],
        tasks: ['protractor:auto']
      }
    },

    uglify: {
      production: {
        options: {
          mangle: false
        },
        files: {
          'build/assets/app.js': 'build/assets/app.js'
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-hashres');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-copy');

  // https://github.com/gruntjs/grunt-contrib-watch/issues/240
  // Run with: grunt switchwatch:target1:target2 to only watch those targets
  grunt.registerTask('switchwatch', function() {
    var targets = Array.prototype.slice.call(arguments, 0);
    Object.keys(grunt.config('watch')).filter(function(target) {
      return !(grunt.util._.indexOf(targets, target) !== -1);
    }).forEach(function(target) {
      grunt.log.writeln('Ignoring ' + target + '...');
      grunt.config(['watch', target], {files: []});
    });
    grunt.task.run('watch');
  });

  //single run tests
  grunt.registerTask('test', ['jshint','test:unit', 'test:e2e']);
  grunt.registerTask('test:unit', ['karma:unit']);
  grunt.registerTask('test:e2e', ['connect:testserver','protractor:singlerun']);

  //autotest and watch tests
  grunt.registerTask('autotest', ['karma:unit_auto']);
  grunt.registerTask('autotest:unit', ['karma:unit_auto']);
  grunt.registerTask('autotest:e2e', ['connect:testserver','shell:selenium','watch:protractor']);

  //coverage testing
  grunt.registerTask('test:coverage', ['karma:unit_coverage']);
  grunt.registerTask('coverage', ['karma:unit_coverage','open:coverage','connect:coverage']);

  //installation-related
  grunt.registerTask('install', ['update','shell:protractor_install']);
  grunt.registerTask('update', ['shell:npm_install']);

  //defaults
  grunt.registerTask('default', ['clean', 'copy', 'compass:dev', 'concat', 'dev']);

  //development
  // grunt.registerTask('dev', ['update', 'connect:devserver', 'open:devserver', 'watch:assets']);
  grunt.registerTask('dev', ['update', 'connect:devserver', 'switchwatch:options:css:js:html']);

  //server daemon
  grunt.registerTask('serve', ['connect:webserver']);

  grunt.registerTask('deploy', ['deploy:prepare', 's3']);
  grunt.registerTask('deploy:test', ['deploy:prepare', 'connect:deploytest']);
  grunt.registerTask('deploy:prepare', ['clean', 'copy:prod', 'compass:prod', 'concat', 'uglify', 'hashres']);
};
